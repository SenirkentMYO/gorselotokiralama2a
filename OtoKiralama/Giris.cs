﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OtoKiralama
{
    public partial class Giris : Form
    {
        public Giris()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<Kullanicilar> Kullananlar = new List<Kullanicilar>();
            OtoKiralamaEntities oe = new OtoKiralamaEntities();
            {
                Kullananlar = oe.Kullanicilars.Where(a => a.KullaniciAdi == txtAdı.Text && a.KullaniciSifre == txtSifre.Text).ToList();

                if (Kullananlar.Count > 0)
                {
                    Form1 f1 = new Form1();
                    f1.Show();

                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Hatalı Bilgiler Var");
                }
            }
        }
    }
}
