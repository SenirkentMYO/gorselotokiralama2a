﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OtoKiralama
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
            
        }

        

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OtoKiralamaEntities otk = new OtoKiralamaEntities())
            {
                //Hocam ben müşteri kayiti veri tabanına bağlamıştım zaten kontrol edersiniz :)
                MüsteriKayit mk = new MüsteriKayit();
                mk.Adı = txtAdi.Text;
                mk.SoyAdi = txtSoyAdi.Text;
                mk.TcNo = txtTcNo.Text;
                mk.DogumTarihi = Convert.ToDateTime(txtDogumTarihi.Text);
                mk.DogumYeri = txtDogumYeri.Text;
                mk.Email = txtMail.Text;
                mk.Adres = txtAdres.Text;
                mk.CepNo = txtCepNo.Text;
                mk.Ehliyet = cmbEhliyet.SelectedIndex.ToString();

                otk.MüsteriKayit.Add(mk);
                otk.SaveChanges();

                MessageBox.Show("Kayıt İşlemi Gerçekleşmiştir");//

            }
        }

        private void çıkışToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            using (OtoKiralamaEntities okl = new OtoKiralamaEntities())
            {
                Kiralama kr = new Kiralama();
                kr.ArabaMarkasi = txtMüstAdi.Text;
                kr.MüsteriSoyAdi = txtMüstSoyAdi.Text;
                kr.TcKimlikNo = txtTcnoo.Text;
                kr.ArabaMarkasi =cmbArbaMrka.SelectedText;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (OtoKiralamaEntities otk = new OtoKiralamaEntities())
            {
                var liste = from mk in otk.MüsteriKayit
                            orderby mk.MusteriID ascending
                            select new
                            {
                                mk.Adı,
                                mk.SoyAdi,
                                mk.TcNo,
                                mk.Adres,
                                mk.CepNo,
                                mk.Email
                            };
                dataGridView1.DataSource = liste.ToList();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (OtoKiralamaEntities ok = new OtoKiralamaEntities())//
            {
                AracKayıt ark = new AracKayıt();
                ark.Plakası = txtPlk.Text;
                ark.Markası = txtMrka.Text;
                ark.Modeli = txtModel.Text;
                ark.KayitTarihi = Convert.ToDateTime(txtKaytTarh.Text);
                ark.Rengi = txtRengi.Text;
                ark.MotorSeriNo = txtMtorSeri.Text;

                ok.AracKayıt.Add(ark);
                ok.SaveChanges();
            }
        }

        private void label26_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
           

        }

        private void button5_Click(object sender, EventArgs e)
        {
            using (OtoKiralamaEntities ok = new OtoKiralamaEntities())
            {
                YekiliServisler ys = new YekiliServisler();
                ys.ServiAdi = txtServisAdi.Text;
                ys.TelNo = txtTel.Text;
                ys.Sehir = txtSehir.Text;
                ys.Adres = textBox5.Text;
                ys.Email = textBox4.Text;

                ok.YekiliServisler.Add(ys);
                ok.SaveChanges();

                MessageBox.Show("İşlem Tamamdır");
            }
        }

        private void txtServisAdi_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            using (OtoKiralamaEntities oe = new OtoKiralamaEntities())
            {
                var listele = from ark in oe.Kiralama
                              orderby ark.KiralamaID ascending
                              select new
                               {
                                   ark.ArabaMarkasi,
                                   ark.EhliyetSüresi,
                                   ark.MüsteriAdi,
                                   ark.MüsteriSoyAdi,
                                   ark.TcKimlikNo


                               };
                dtgKiralama.DataSource = listele.ToList();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            OtoKiralamaEntities db = new OtoKiralamaEntities();
            List<MüsteriKayit> MüsteriListesi = db.MüsteriKayit.OrderByDescending(b => b.MusteriID).ToList();
            MüsteriKayit m = db.MüsteriKayit.OrderByDescending(b => b.MusteriID).First();

            dataGridView1.DataSource = MüsteriListesi;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            OtoKiralamaEntities db = new OtoKiralamaEntities();
            List<MüsteriKayit> MüsteriListesi = db.MüsteriKayit.ToList();

            var linqliste = (from m in MüsteriListesi
                             where m.TcNo == textBox1.Text
                             select new
                             {
                                 MüsteriAdı = m.Adı,
                                 MüsteriSoyAdı = m.SoyAdi,
                                 MüsteriTCNo = m.TcNo,
                                 MüsterDogumTarihi = m.DogumTarihi,
                                 MüsteriDogumYeri = m.DogumYeri,
                                 MüsteriEmail = m.Email,
                                 MüsteriTel = m.CepNo
                             }).ToList();

            dataGridView1.DataSource = linqliste;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            OtoKiralamaEntities db = new OtoKiralamaEntities();
            List<MüsteriKayit> Müsteriliste = db.MüsteriKayit.Take(1).ToList();
            dataGridView1.DataSource = Müsteriliste;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            OtoKiralamaEntities db = new OtoKiralamaEntities();
            List<MüsteriKayit> Müsteriler = db.MüsteriKayit.ToList();
            dataGridView1.DataSource = Müsteriler;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            OtoKiralamaEntities db = new OtoKiralamaEntities();
            int Id=int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            MüsteriKayit mk = db.MüsteriKayit.Where(a => a.MusteriID == Id).First();

            TextboxaAtama ta = new TextboxaAtama(mk);
            ta.Show();
      }

        private void button11_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells["MusteriID"].Value);
            using (OtoKiralamaEntities db = new OtoKiralamaEntities())
            {
                var liste = from m in db.MüsteriKayit
                            where m.MusteriID == id
                            select m;
                liste.ToList().ForEach(a => db.MüsteriKayit.Remove(a));
                db.SaveChanges();
                MessageBox.Show("Kayıt Silindi");
            }
        }
   
    }
}